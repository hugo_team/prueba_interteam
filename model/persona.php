<?php
class persona{
	private $pdo;

    public $id;
    public $nombre;
	public $apellido_p;
	public $apellido_m;
	public $fecha_nac;
	public $id_estado_civil;
	public $telefono;
	public $correo;
	public $hijos;
	public $calle;
	public $num_ext;
	public $num_int;
	public $colonia;
	public $cp;
	public $ciudad;
	public $municipio;
	public $id_estado;
	public $id_pais;

	public function __CONSTRUCT(){
		try{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Listar(){
		try{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM persona p
					JOIN estado_civil ec on ec.id_estado_civil = p.id_estado_civil
					JOIN estados e on e.id_estado = p.id_estado
					JOIN pais pa on pa.id_pais = p.id_pais");
			$stm->execute();		
				
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Obtener($id){
		try{

			$result = array();
			if (isset($id)){
				$stm = $this->pdo
						->prepare("SELECT * FROM persona WHERE id = ?");			          
				$stm->execute(array($id));
				$result['datos'] = $stm->fetch(PDO::FETCH_OBJ);	
			}else{			
				$persona = new persona();
				$persona->id = '';
				$persona->nombre = '';
				$persona->apellido_p = '';
				$persona->apellido_m = '';
				$persona->fecha_nac = ''; 
				$persona->id_estado_civil = ''; 
				$persona->telefono = '';
				$persona->correo = '';  
				$persona->hijos = '0';            
				$persona->calle = ''; 
				$persona->num_ext = ''; 
				$persona->num_int = ''; 
				$persona->colonia = ''; 
				$persona->cp = ''; 
				$persona->ciudad = '';
				$persona->municipio = '';
				$persona->id_estado = '';
				$persona->id_pais = '1';

				$result['datos'] = $persona;
			}
			

			$edo_civ = $this->pdo->prepare("SELECT * FROM estado_civil");
			$edo_civ->execute();
			$result['estados_civil'] = $edo_civ->fetchAll(PDO::FETCH_OBJ);

			$edos = $this->pdo->prepare("SELECT * FROM estados");
			$edos->execute();
			$result['estados'] = $edos->fetchAll(PDO::FETCH_OBJ);

			$pais = $this->pdo->prepare("SELECT * FROM pais");
			$pais->execute();
			$result['paises'] = $pais->fetchAll(PDO::FETCH_OBJ);

			return $result;
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function Eliminar($id){
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM persona WHERE id = ?");			          

			$stm->execute(array($id));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function Actualizar($data){
		try{
			$sql = "UPDATE persona SET 						
						nombre           = ?, 
						apellido_p       = ?,
						apellido_m       = ?,
						fecha_nac        = ?,
						id_estado_civil  = ?,
						telefono         = ?,
                        correo           = ?,
                        hijos            = ?,
						calle            = ?,
						num_ext          = ?,
						num_int	         = ?,
						colonia          = ?,
						cp               = ?,
						ciudad           = ?,
						municipio        = ?,
						id_estado        = ?,
						id_pais          = ?
						
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->nombre           , 
						$data->apellido_p       ,
						$data->apellido_m       ,
						$data->fecha_nac        ,
						$data->id_estado_civil  ,
						$data->telefono         ,
						$data->correo           ,
						$data->hijos            ,
						$data->calle            ,
						$data->num_ext          ,
						$data->num_int	         ,
						$data->colonia          ,
						$data->cp               ,
						$data->ciudad           ,
						$data->municipio        ,
						$data->id_estado        ,
						$data->id_pais          ,
						$data->id

					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function Registrar(persona $data){
		try{
		$sql = "INSERT INTO persona (nombre, apellido_p, apellido_m, fecha_nac, id_estado_civil, telefono, correo, hijos, calle, num_ext, num_int, colonia, cp, ciudad, municipio, id_estado, id_pais) 
		        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$this->pdo->prepare($sql)
		     ->execute(
				array(
					 
                    $data->nombre           , 
					$data->apellido_p       ,
					$data->apellido_m       ,
					$data->fecha_nac        ,
					$data->id_estado_civil  ,
					$data->telefono         ,
					$data->correo           ,
					$data->hijos            ,
					$data->calle            ,
					$data->num_ext          ,
					$data->num_int	         ,
					$data->colonia          ,
					$data->cp               ,
					$data->ciudad           ,
					$data->municipio        ,
					$data->id_estado        ,
					$data->id_pais          
                   
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}