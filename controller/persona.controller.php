<?php
require_once 'model/persona.php';

class personaController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new persona();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/persona/persona.php';
       
    }
    
    public function Crud(){
        $persona = new persona();
        $id = isset($_REQUEST['id'])? $_REQUEST['id'] : null;
        $persona = $this->model->Obtener($id);

        require_once 'view/header.php';
        require_once 'view/persona/persona-editar.php';
        
    }
    
    public function Guardar(){
        $persona = new persona();
   
        $persona->id = $_REQUEST['id'];
        $persona->nombre = $_REQUEST['nombre'];
        $persona->apellido_p = $_REQUEST['apellido_p'];
        $persona->apellido_m = $_REQUEST['apellido_m'];
        $persona->fecha_nac = $_REQUEST['fecha_nac']; 
        $persona->id_estado_civil = $_REQUEST['estado_civil']; 
        $persona->telefono = $_REQUEST['telefono'];
        $persona->correo = $_REQUEST['correo'];  
        $persona->hijos = $_REQUEST['hijos'];            
        $persona->calle = $_REQUEST['calle']; 
        $persona->num_ext = $_REQUEST['num_ext']; 
        $persona->num_int = $_REQUEST['num_int']; 
        $persona->colonia = $_REQUEST['colonia']; 
        $persona->cp = $_REQUEST['cp']; 
        $persona->ciudad = $_REQUEST['ciudad'];
        $persona->municipio = $_REQUEST['municipio'];
        $persona->id_estado = $_REQUEST['estado'];
        $persona->id_pais = $_REQUEST['pais'];
        
        $persona->id > 0 
            ? $this->model->Actualizar($persona)
            : $this->model->Registrar($persona);
        
        header('Location: index.php');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php');
    }
}