-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.35-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para personas
CREATE DATABASE IF NOT EXISTS `personas` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `personas`;

-- Volcando estructura para tabla personas.estados
CREATE TABLE IF NOT EXISTS `estados` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla personas.estados: ~32 rows (aproximadamente)
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` (`id_estado`, `estado`) VALUES
	(1, 'Aguascalientes'),
	(2, 'Baja California'),
	(3, 'Baja California Sur'),
	(4, 'Campeche'),
	(5, 'Coahuila de Zaragoza'),
	(6, 'Colima'),
	(7, 'Chiapas'),
	(8, 'Chihuahua'),
	(9, 'Ciudad de México'),
	(10, 'Durango'),
	(11, 'Guanajuato'),
	(12, 'Guerrero'),
	(13, 'Hidalgo'),
	(14, 'Jalisco'),
	(15, 'México'),
	(16, 'Michoacán de Ocampo'),
	(17, 'Morelos'),
	(18, 'Nayarit'),
	(19, 'Nuevo León'),
	(20, 'Oaxaca de Juárez'),
	(21, 'Puebla'),
	(22, 'Querétaro'),
	(23, 'Quintana Roo'),
	(24, 'San Luis Potosí'),
	(25, 'Sinaloa'),
	(26, 'Sonora'),
	(27, 'Tabasco'),
	(28, 'Tamaulipas'),
	(29, 'Tlaxcala'),
	(30, 'Veracruz de Ignacio de la Llave'),
	(31, 'Yucatán'),
	(32, 'Zacatecas');
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;

-- Volcando estructura para tabla personas.estado_civil
CREATE TABLE IF NOT EXISTS `estado_civil` (
  `id_estado_civil` int(11) NOT NULL AUTO_INCREMENT,
  `estado_civil` varchar(45) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`id_estado_civil`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla personas.estado_civil: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `estado_civil` DISABLE KEYS */;
INSERT INTO `estado_civil` (`id_estado_civil`, `estado_civil`) VALUES
	(1, 'Soltero'),
	(2, 'Casado'),
	(3, 'Union Libre'),
	(4, 'Viudo'),
	(5, 'Divorciado');
/*!40000 ALTER TABLE `estado_civil` ENABLE KEYS */;

-- Volcando estructura para tabla personas.pais
CREATE TABLE IF NOT EXISTS `pais` (
  `id_pais` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(45) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla personas.pais: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` (`id_pais`, `pais`) VALUES
	(1, 'México');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;

-- Volcando estructura para tabla personas.persona
CREATE TABLE IF NOT EXISTS `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `apellido_p` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `apellido_m` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `fecha_nac` date NOT NULL,
  `id_estado_civil` int(11) NOT NULL,
  `correo` varchar(50) CHARACTER SET latin1 NOT NULL,
  `hijos` int(11) NOT NULL DEFAULT '0',
  `telefono` varchar(20) CHARACTER SET latin1 NOT NULL,
  `calle` varchar(255) CHARACTER SET latin1 NOT NULL,
  `num_ext` int(11) DEFAULT NULL,
  `num_int` int(11) DEFAULT NULL,
  `colonia` varchar(255) CHARACTER SET latin1 NOT NULL,
  `cp` varchar(45) CHARACTER SET latin1 NOT NULL,
  `ciudad` varchar(45) CHARACTER SET latin1 NOT NULL,
  `municipio` varchar(255) CHARACTER SET latin1 NOT NULL,
  `id_estado` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estado_civil` (`id_estado_civil`),
  KEY `estado` (`id_estado`),
  KEY `pais` (`id_pais`),
  CONSTRAINT `estado` FOREIGN KEY (`id_estado`) REFERENCES `estados` (`id_estado`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `estado_civil` FOREIGN KEY (`id_estado_civil`) REFERENCES `estado_civil` (`id_estado_civil`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pais` FOREIGN KEY (`id_pais`) REFERENCES `pais` (`id_pais`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla personas.persona: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` (`id`, `nombre`, `apellido_p`, `apellido_m`, `fecha_nac`, `id_estado_civil`, `correo`, `hijos`, `telefono`, `calle`, `num_ext`, `num_int`, `colonia`, `cp`, `ciudad`, `municipio`, `id_estado`, `id_pais`) VALUES
	(1, 'Walter Hugo', 'Guillén', 'Carrillo', '1986-11-02', 2, 'hugo.guc@gmail.com', 1, '5522271014', 'Brecha al Chiquihuite', 13, 7, 'Tlalpexco', '07188', 'CDMX', 'G.A.M.', 9, 1);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
