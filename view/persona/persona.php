<h1 class="page-header">Prueba técnica Interteam </h1>
<h2>Sistema de Alta, Baja y Modificacion de personas</h2>

    <a class="btn btn-success pull-right" href="?c=persona&a=Crud">Agregar</a>
<br><br><br>

<table class="table  table-striped  table-hover" id="tabla">
    <thead style="background-color: #6440a2; color: #fff;">
        <tr>        
            <th style="width:180px;">Nombre </th>
            <th>Fecha de nac.</th>
            <th>Edad</th>                           
            <th>Edo. Civil</th>
            <th>Telefono</th> 
            <th>Correo</th>
            <th>Hijos</th>
            <th>Domicilio</th>                    
            <th style="width:60px;"></th>
            <th style="width:60px;"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>         
            <td><?php echo $r->nombre.' '.$r->apellido_p. ' '.$r->apellido_m; ?> </td>
            <td><?php echo $r->fecha_nac; ?></td>
            <td><?php echo ((new DateTime($r->fecha_nac))->diff(new DateTime("NOW")) )->y . ' años ';?></td>            
            <td><?php echo $r->estado_civil; ?></td>
            <td><?php echo $r->telefono; ?></td>
            <td><?php echo $r->correo; ?></td>
            <td><?php echo $r->hijos; ?></td>
            <td><?php echo $r->calle .' Ext. '.$r->num_ext .' Int. '. $r->num_int .'<br> Col.'.$r->colonia.',  '.$r->municipio .',<br>'.$r->estado.'<br>Ciuidad '.$r->ciudad.'<br>' .$r->pais ; ?></td>
            <td>
                <a  class="btn btn-warning" href="?c=persona&a=Crud&id=<?php echo $r->id; ?>">Editar</a>
            </td>
            <td>
                <a  class="btn btn-danger" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=persona&a=Eliminar&id=<?php echo $r->id; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 

</body>
<script  src="assets/js/datatable.js">  

</script>


</html>
