<h1 class="page-header">
    <?php echo $persona['datos']->id != null ? $persona['datos']->nombre.' '.$persona['datos']->apellido_p.' '.$persona['datos']->apellido_m : 'Nuevo Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=persona">Todos</a></li>
  <li class="active"><?php echo $persona['datos']->id != null ? $persona['datos']->nombre : 'Nuevo Registro'; ?></li>
</ol>

<form id="frm-persona" action="?c=persona&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $persona['datos']->id; ?>" />
      <div class="form-group">
        
    </div>
    
    <div class="form-group">
        <div class="col-md-4">
            <label>Nombre(s)</label>
            <input type="text" name="nombre" value="<?php echo $persona['datos']->nombre; ?>" class="form-control" placeholder="Ingrese su nombre" required>
        </div>

        <div class="col-md-4">
            <label>Apellido paterno</label>
            <input type="text" name="apellido_p" value="<?php echo $persona['datos']->apellido_p; ?>" class="form-control" placeholder="Ingrese su apellido paterno" required>
        </div>

        <div class="col-md-4">
            <label>Apellido materno</label>
            <input type="text" name="apellido_m" value="<?php echo $persona['datos']->apellido_m; ?>" class="form-control" placeholder="Ingrese su apellido materno" required>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-md-4">
            <label>Fecha de Nacimiento</label>
            <input type="text" name="fecha_nac" value="<?php echo $persona['datos']->fecha_nac; ?>" class="form-control datepicker" placeholder="Ingrese su fecha de nacimiento" required autocomplete="off">
        </div>
        <div class="col-md-4">
            <label>Estado Civil</label>
            <select class="form-control" name="estado_civil" required>
                <option>Seleccione</option>
                <?php foreach($persona['estados_civil'] as $estado_civil): ?>
                <option value="<?php echo $estado_civil->id_estado_civil; ?>" <?php echo $persona['datos']->id_estado_civil == $estado_civil->id_estado_civil ? 'selected': '';  ?> >
                    <?php echo $estado_civil->estado_civil; ?>
                </option>
                <?php endforeach; ?>
            </select>        
        </div>
        <div class="col-md-4">
            <label>Telefono</label>
            <input type="text" name="telefono" value="<?php echo $persona['datos']->telefono; ?>" class="form-control" placeholder="Ingrese su telefono" required  pattern="[0-9]{10}">
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-4">
            <label>Correo</label>
            <input type="email" name="correo" value="<?php echo $persona['datos']->correo; ?>" class="form-control" placeholder="Ingrese su correo electrónico" required>
        </div>

        <div class="col-md-4">
            <label>Número de hijos</label>
            <input type="number" name="hijos" min="0" value="<?php echo $persona['datos']->hijos; ?>" class="form-control" placeholder="Ingrese cuántos hijos tiene"  required pattern="[0-9]">
        </div>
    </div>
    <div style="clear:both;"></div>
    <hr />

    <h4>Domicilio</h4>

    <div class="form-group">
        <div class="col-md-6">
            <label>Calle</label>
            <input type="text" name="calle" value="<?php echo $persona['datos']->calle; ?>" class="form-control" placeholder="Ingrese la calle de su domicilio" required>
        </div>
        <div class="col-md-3">
            <label>Núm exterior</label>
            <input type="text" name="num_ext" value="<?php echo $persona['datos']->num_ext; ?>" class="form-control" placeholder="Ingrese el número exterior" required>
        </div>
        <div class="col-md-3">
            <label>Núm interior</label>
            <input type="text" name="num_int" value="<?php echo $persona['datos']->num_int; ?>" class="form-control" placeholder="Ingrese el número interior" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-4">
            <label>Colonia</label>
            <input type="text" name="colonia" value="<?php echo $persona['datos']->colonia; ?>" class="form-control" placeholder="Ingrese su colonia" required>
        </div>
        <div class="col-md-4">
            <label>Código postal</label>
            <input type="text" name="cp" value="<?php echo $persona['datos']->cp; ?>" class="form-control" placeholder="Ingrese su código postal" required  pattern="[0-9]{5}">
        </div>
        <div class="col-md-4">
            <label>Ciudad</label>
            <input type="text" name="ciudad" value="<?php echo $persona['datos']->ciudad; ?>" class="form-control" placeholder="Ingrese su ciudad" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-4">
            <label>Delegación o Municipio</label>
            <input type="text" name="municipio" value="<?php echo $persona['datos']->municipio; ?>" class="form-control" placeholder="Ingrese su delegación o municipio" required>
        </div>
        <div class="col-md-4">
            <label>Estado</label>            
            <select class="form-control" name="estado" required>
                <option>Seleccione</option>
                <?php foreach($persona['estados'] as $estado): ?>
                <option value="<?php echo $estado->id_estado; ?>" <?php echo $persona['datos']->id_estado == $estado->id_estado ? 'selected': '';  ?> >
                    <?php echo $estado->estado; ?>
                </option>
                <?php endforeach; ?>
            </select> 
        </div>
        <div class="col-md-4">
            <label>País</label>
            <select class="form-control" name="pais" required>
                <option>Seleccione</option>
                <?php foreach($persona['paises'] as $pais): ?>
                <option value="<?php echo $pais->id_pais; ?>" <?php echo $persona['datos']->id_pais == $pais->id_pais ? 'selected': '';  ?> >
                    <?php echo $pais->pais; ?>
                </option>
                <?php endforeach; ?>
            </select> 
        </div>
    </div>

    <div style="clear:both;"></div>

    <hr />
    
    <div class="text-right">
        <button class="btn btn-success">Guardar</button>
    </div>
</form>
<script src="assets/js/datepicker.js" type="text/javascript"></script>
<script>
    $(document).ready(function(){
       
        $("#frm-persona").submit(function(){
            return $(this).validate();
        });
    });
</script>